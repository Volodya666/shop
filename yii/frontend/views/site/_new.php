<div class="panel panel-default padding-20">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-3">
                <h1 class="muted"><span class="glyphicon glyphicon-stats"></span> Топ товари</h1>
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                    <div class="page-item parent-new-link-prev">
                        <a class="page-link new-link-prev fa fa-arrow-circle-left" href="javascript:void(0)"></a>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: right">
                    <div class="page-item parent-new-link-next">
                        <a class="page-link new-link-next fa fa-arrow-circle-right" href="javascript:void(0)"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="load-ajax-new">
            <div class="new-wrap wrap-main-item">

            </div>
        </div>
    </div>
</div>

<?= $this->render("//common/_template_card", ['templateId' => 'tpl-new']) ?>