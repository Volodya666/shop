<?php

namespace backend\controllers;

use common\models\Categories;
use common\models\ShopCategories;

class MergeCategoriesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $tree = (new Categories)->getTree();

        $shopCategories =  ShopCategories::find()
            ->select("shop_categories.*")
            ->leftJoin('category_items', 'category_items.shop_category_id = shop_categories.shop_category_id')
            ->where(['category_items.shop_category_id' => null])
            ->all();

        return $this->render('index', ['tree' => $tree, 'shopCategories' => $shopCategories]);
    }
}
