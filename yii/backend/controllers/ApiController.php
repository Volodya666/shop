<?php
/**
 * Created by PhpStorm.
 * User: key
 * Date: 06.10.2019
 * Time: 20:51
 */

namespace backend\controllers;

use common\models\Categories;
use common\models\CategoryItems;
use common\models\ShopCategories;

class ApiController extends \yii\web\Controller
{
    /**
     * @return bool
     */
    public function actionChangesJsTree()
    {
        $request = \Yii::$app->request;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $parentId = $request->get('parentId');
        $nodeId = $request->get('nodeId');

        $model = Categories::findOne($nodeId);
        $model->parent_id = $parentId;

        return $model->save();
    }

    public function actionPopular()
    {
        $request = \Yii::$app->request;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $categoryId = $request->get('categoryId');
        $model = Categories::findOne($categoryId);
        return $model->popular();
    }

    /**
     * @return bool
     */
    public function actionSetCategoryItems()
    {
        $request = \Yii::$app->request;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new CategoryItems();
        $model->category_id = $request->get('categoryId');
        $model->shop_category_id = $request->get('shopCategoryId');

        return $model->save();
    }

    public function actionGetCategoryItems($shopCategoryId = null)
    {
        $shopCategories =  ShopCategories::find()
            ->select("shop_categories.*")
            ->leftJoin('category_items', 'category_items.shop_category_id = shop_categories.shop_category_id')
            ->where(['category_items.category_id' => $shopCategoryId])
            ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $shopCategories;
    }

    /**
     * @param null $categoryId
     * @return string
     */
    public function actionGetCategoryImg($categoryId = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Categories::findOne($categoryId);
        return $model->img;
    }

    /**
     * @return bool
     */
    public function actionSetCategoryImg()
    {
        $request = \Yii::$app->request;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $categoryId = $request->get('categoryId');
        $img = $request->get('img');
        $model = Categories::findOne($categoryId);
        $model->img = $img;
        return $model->save();
    }
}