<div class="panel panel-default padding-20">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-3">
                <h1 class="muted"><span class="glyphicon glyphicon-flash"></span> Тавари по акци</h1>
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                    <div class="page-item parent-sales-link-prev">
                        <a class="page-link sales-link-prev fa fa-arrow-circle-left" href="javascript:void(0)"></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="page-item parent-sales-link-next" style="text-align: right">
                        <a class="page-link sales-link-next fa fa-arrow-circle-right" href="javascript:void(0)"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="load-ajax-sales">
            <div class="sales-wrap wrap-main-item">

            </div>
        </div>
    </div>
</div>

<?= $this->render("//common/_template_card", ['templateId' => 'tpl-sales']) ?>