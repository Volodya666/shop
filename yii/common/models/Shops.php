<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property int $shop_id
 * @property string $created_at
 * @property string $name
 * @property string $deep_link_hash
 * @property string $logo
 * @property int $fl_use
 * @property int $show_in_main_page
 *
 * @property ShopCategories[] $shopCategories
 */
class Shops extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['name', 'deep_link_hash', 'logo', 'fl_use', 'show_in_main_page'], 'required'],
            [['fl_use', 'show_in_main_page'], 'integer'],
            [['name', 'deep_link_hash', 'logo'], 'string', 'max' => 255],
            [['deep_link_hash'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shop_id' => 'Shop ID',
            'created_at' => 'Created At',
            'name' => 'Name',
            'deep_link_hash' => 'Deep Link Hash',
            'logo' => 'Logo',
            'fl_use' => 'Fl Use',
            'show_in_main_page' => 'Show In Main Page',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategories()
    {
        return $this->hasMany(ShopCategories::className(), ['shop_id' => 'shop_id']);
    }

    /**
     * @return array
     */
    public function getDepLink()
    {
        $model = self::find()->all();

        $result = [];
        foreach ($model as $item) {
            $result[$item->shop_id] = $item->deep_link_hash;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getLogoList()
    {
        $model = self::find()->limit(20)->all();

        $result = [];
        foreach ($model as $item) {
            $result[$item->shop_id] = $item->logo;
        }

        return $result;
    }
}
