<?php
namespace frontend\controllers;

use common\models\Offers;

use yii\web\Controller;
use yii\data\Pagination;
use yii\web\Response;

class ApiController extends Controller
{
    /**
     * @return array
     */
    public function actionSales()
    {
        $querySales = Offers::find()->where('sales IS NOT NULL');
        $countQuerySales = clone $querySales;
        $pagesSale = new Pagination(['totalCount' => $countQuerySales->count(), 'defaultPageSize' => 6]);
        $modelsSale = $querySales->offset($pagesSale->offset)
            ->limit($pagesSale->limit)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return (new Offers())->getSalesList($modelsSale);
    }

    /**
     * @return array
     */
    public function actionNew()
    {
        $queryTop = Offers::find()->where('new IS NOT NULL');
        $countQueryTop = clone $queryTop;
        $pagesTop = new Pagination(['totalCount' => $countQueryTop->count(), 'defaultPageSize' => 6]);
        $modelsTop = $queryTop->offset($pagesTop->offset)
            ->limit($pagesTop->limit)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return (new Offers())->getSalesList($modelsTop);
    }

    /**
     * @param string $q
     * @param int $pages
     * @return array
     */
    public function actionSearch($q = '', $pages = 0)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return (new Offers())->getDropListByName($q, $pages);
    }

    /**
     * @param array $data
     * @return array|string
     */
    public function actionOffers(array $data = [])
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Offers $model */
        $model = (new  Offers)->getSearchOffers($data);
        return (new  Offers)->getSalesList($model);
    }
}