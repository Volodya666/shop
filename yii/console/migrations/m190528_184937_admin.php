<?php

use yii\db\Migration;

/**
 * Class m190528_184937_admin
 */
class m190528_184937_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('\vO7DJ1_a5+Rto;\8Ry]'),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_reset_token' => Yii::$app->security->generateRandomString() . '_' . time(),
            'verification_token' => Yii::$app->security->generateRandomString() . '_' . time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
