<?php

use yii\db\Migration;

/**
 * Class m190605_145506_banners
 */
class m190605_145506_banners extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%banners}}', [
            'banner_id' => $this->primaryKey(),
            'banner' => $this->string(),
            'date_created' => $this->date(),
            'is_show' => $this->boolean(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banners}}');
    }
}
