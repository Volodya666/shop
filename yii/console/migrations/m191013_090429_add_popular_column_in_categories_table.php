<?php

use yii\db\Migration;

/**
 * Class m191013_090429_add_popular_column_in_categories_table
 */
class m191013_090429_add_popular_column_in_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'popular', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('categories', 'popular');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191013_090429_add_popular_column_in_categories_table cannot be reverted.\n";

        return false;
    }
    */
}
