<?php

namespace common\models;

use common\system\Helper;
use Yii;

/**
 * This is the model class for table "offers".
 *
 * @property int $offer_id
 * @property int $id
 * @property int $id_category
 * @property string $name
 * @property string $picture
 * @property string $all_images
 * @property double $price
 * @property double $sale_price
 * @property string $currency
 * @property string $lang
 * @property double $commision_rate
 * @property int $store_id
 * @property string $store_title
 * @property int $orders_count
 * @property string $evaluatescore
 * @property string $prices
 * @property string $sale_prices
 * @property string $description
 *
 * @property ShopCategories $category
 */
class Offers extends \yii\db\ActiveRecord
{
    const TYPE_TOP = 1;
    const TYPE_SALES = 2;
    const PRICE_DESCENDING = 1;
    const PRICE_ASCENDING = 2;

    CONST DEFAULT_LIMIT = 30;

    public $_search;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'id' => 'ID',
            'id_category' => 'Id Category',
            'name' => 'Name',
            'picture' => 'Picture',
            'all_images' => 'All Images',
            'price' => 'Price',
            'sale_price' => 'Sale Price',
            'currency' => 'Currency',
            'lang' => 'Lang',
            'commision_rate' => 'Commision Rate',
            'store_id' => 'Store ID',
            'store_title' => 'Store Title',
            'orders_count' => 'Orders Count',
            'evaluatescore' => 'Evaluatescore',
            'prices' => 'Prices',
            'sale_prices' => 'Sale Prices',
            'description' => 'Description',
        ];
    }

    /**
     * @param $params
     * @param $id
     * @param bool $top
     */
    public function getParams($params, $id, $top = false)
    {
        $this->id = (int)Helper::getFromMap($params, 'id');
        $this->id_category = (int)Helper::getFromMap($params, 'id_category');
        $this->name = Helper::getFromMap($params, 'name');
        $this->picture = Helper::getFromMap($params, 'picture');
        $this->all_images = json_encode(Helper::getFromMap($params, 'all_images'));
        $this->price = Helper::getFromMap($params, 'price');
        $this->sale_price = Helper::getFromMap($params, 'sale_price');
        $this->currency = Helper::getFromMap($params, 'currency');
        $this->lang = Helper::getFromMap($params, 'lang');
        $this->commision_rate = Helper::getFromMap($params, 'commission_rate');
        $this->store_id = (int)Helper::getFromMap($params, 'store_id');
        $this->store_title = Helper::getFromMap($params, 'store_title');
        $this->orders_count = (int)Helper::getFromMap($params, 'orders_count');
        $this->evaluatescore = Helper::getFromMap($params, 'evaluatescore');
        $this->sale_prices = json_encode(Helper::getFromMap($params, 'sale_prices'));
        $this->description = Helper::getFromMap($params, 'description');
        $this->url = Helper::getFromMap($params, 'url');
        $this->prices = json_encode(Helper::getFromMap($params, 'prices'));
        $this->shop_id = $id;

        if ($this->sale_price && ($this->price - $this->sale_price) > 1) {
            $this->sales = true;
        }

        if ($top) {
            $this->new = true;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategory()
    {
        return $this->hasOne(ShopCategories::className(), ['id' => 'id_category', 'shop_id' => 'shop_id']);
    }

    /**
     * @param $data
     * @return array
     */
    public function getSalesList($data)
    {
        $logos = (new Shops())->getLogoList();
        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'sales' => $item->sales,
                'new' => $item->new,
                'name' => $item->name,
                'picture' => $item->picture,
                'shop_id' => $item->shop_id,
                'url' => $item->url,
                'price' => $item->price,
                'sale_price' => $item->sale_price,
                'logos' => Helper::getFromMap($logos, $item->shop_id)
            ];
        }

        return $result;
    }

    /**
     * @param $q
     * @param $pages
     * @param bool $m
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getDropListByName($q, $pages, $m = false)
    {
        $defaultLimit = 30;
        $model = Offers::find()
            ->where(['like', 'name', '%' . $q . '%', false])
            ->offset($pages * $defaultLimit)
            ->limit($defaultLimit)
            ->all();

        //
        if ($m) {
            return $model;
        }

        $count = Offers::find()
            ->where(['like', 'name', '%' . $q . '%', false])
            ->count();

        $logos = (new Shops())->getLogoList();
        $items = [];

        /** @var Offers $item */
        foreach ($model as $item) {
            $items[] = $item->prepare($logos);
        }

        return [
            'incomplete_results' => false,
            'items' => $items,
            'total_count' => $count
        ];
    }

    /**
     * @param $logos
     * @return array
     */
    public function prepare($logos)
    {
        return [
            'name' => $this->name,
            'picture' => $this->picture,
            'price' => $this->sale_price ? $this->sale_price : $this->price,
            'url' => $this->url,
            'logos' => Helper::getFromMap($logos, $this->shop_id)
        ];
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_TOP => 'Топ товары',
            self::TYPE_SALES => 'Твары по акции'
        ];
    }

    /**
     * @return array
     */
    public function getPriceType()
    {
        return [
            self::PRICE_DESCENDING => 'Цена по убыванию',
            self::PRICE_ASCENDING => 'Цена по возрастанию',
        ];
    }

    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSearchOffers($params)
    {
        $q = Helper::getFromMap($params, 'value');
        $type = Helper::getFromMap($params, 'type');
        $priceMin = Helper::getFromMap($params, 'price_min');
        $priceMax = Helper::getFromMap($params, 'price_max');
        $sort = Helper::getFromMap($params, 'sort');
        $pages = Helper::getFromMap($params, 'page', 1);
        $categoryId = Helper::getFromMap($params, 'categoryId');

        if (!$q && !$categoryId) {
            return [];
        }

        $query = Offers::find();

        $query->where(['like', 'offers.name', '%' . $q . '%', false])
            ->offset(($pages - 1)* self::DEFAULT_LIMIT)
            ->limit(self::DEFAULT_LIMIT);

        if ($categoryId) {
            $query->leftJoin('shop_categories', '`shop_categories`.`id` = `offers`.`id_category` and `shop_categories`.`shop_id` = `offers`.`shop_id`')
                ->leftJoin('category_items', 'category_items.shop_category_id = shop_categories.shop_category_id')
                ->leftJoin('categories', 'categories.category_id = category_items.category_id');
            $query->andWhere(['categories.category_id' => $categoryId]);
        }

        if ($type && $type == self::TYPE_TOP) {
            $query->andWhere('offers.new IS NOT NULL');
        }

        if ($type && $type == self::TYPE_SALES) {
            $query->andWhere('offers.sales IS NOT NULL');
        }

        if (Helper::createPrice($priceMin)) {
            $query->andWhere('CAST(offers.price AS INT) >= :price_min', [':price_min' => Helper::createPrice($priceMin)]);
        }

        if (Helper::createPrice($priceMax)) {
            $query->andWhere('CAST(offers.price AS INT) <= :price_max', [':price_max' => Helper::createPrice($priceMax)]);
        }

        if ($sort) {
            $s = $sort == self::PRICE_DESCENDING ? 'ASC' : 'DESC';
            $query->orderBy("CAST(offers.price AS INT) {$s}");
        }

//        echo $query->createCommand()->sql; die;

        return $query->all();
    }
}
