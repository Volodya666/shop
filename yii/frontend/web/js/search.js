var offer = (function () {

    var offerWrap, wrapInput, search, _value, _offset, _page, categoryId;

    /**
     *
     */
    function init() {
        initParams();
        bind();
        initSearchSelect();
        getList();
    }

    /**
     *
     * @param param
     */
    function initVariable(param) {
        _value = param.value;
        categoryId = param.categoryId;
    }

    /**
     *
     */
    function initParams() {
        wrapInput = $('#w4');
        offerWrap = $('.offer-wrap');
    }

    /**
     *
     */
    function bind() {
        $(document).on('keypress', '.select2-search__field', function () {
            var elem = $(this);
            setTimeout(function () {
                search = elem.val();
            }, 0);
        }).on('blur', '.select2-search__field', function () {
            initSearchSelect(search);
        }).on('click', '.search-offers', function () {
            _page = 1;
            initSearchSelect(search);
            $('.select2-search__field').blur();
            setTimeout(function () {
                if (!wrapInput.val()) {
                    return true;
                }
                getList();
            }, 0);

        }).on('blur', '.param-price-min, .param-price-max', function () {
            getList();
        }).on('change', '.param-type-price', function () {
            getList();
        }).on('change', '#w0', function () {
            getList();
        }).on('click', '.add-offer', function () {
            _page = _page ? _page + 1 : 2;
            getList(true);
        });
    }

    /**
     *
     */
    function initSearchSelect(val) {

        if(val){
            _value = val;
        }
        wrapInput.html('<option value="' + _value + '">' + _value + '</option>');
        wrapInput.val(_value);
    }

    /**
     *
     * @param append
     */
    function getList(append) {
        form.get('/api/offers', {data: collectData()}, {
            beforeSend: function () {
                offerWrap.addClass('loading');
            },
            success: function (data) {
                var addOffer = $('.add-offer');
                if (!append) {
                    if (!data || data.length < 1) {
                        offerWrap.removeClass('loading');
                        $('.offers-wrap').html('<div class="alert alert-success" role="alert"> Резултат не найден</div>');
                        addOffer.hide();
                        return;
                    }
                }
                addOffer.show();
                showOffer(data, append);
                offerWrap.removeClass('loading');
            }
        });
    }

    /**
     *
     * @param params
     * @param append
     */
    function showOffer(params, append) {
        var data = {data: params},
            wrap = $('.offers-wrap'),
            html = _.template($('#tpl-search').html(), data)(data);

        if (append) {
            wrap.append(html);
        } else {
            wrap.html(html);
        }

        form.tooltips();
    }

    /**
     *
     * @returns {{value: *, type: (*|jQuery), price_min: (*|jQuery), price_max: (*|jQuery), sort: (*|jQuery)}}
     */
    function collectData() {
        return {
            value: search || _value,
            type: $('.param-type-offer').val(),
            price_min: $('.param-price-min').val(),
            price_max: $('.param-price-max').val(),
            sort: $('.param-type-price').val(),
            page: _page,
            categoryId: categoryId
        }
    }

    //
    $(document).ready(function () {
        init();
    });

    //
    return {

        /**
         *
         * @param value
         */
        initVariable: function (value) {
            initVariable(value);
        }
    };
})();