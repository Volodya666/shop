<?php
namespace common\system;

use Yii;

class Helper
{

    /**
     * @param $map
     * @param $key
     * @param null $default
     * @return null
     */
    public static function getFromMap($map, $key, $default = null)
    {
        return (isset($map[$key])) ? $map[$key] : $default;
    }

    /**
     * @param $price
     * @return mixed|null
     */
    public static function createPrice($price)
    {
        if (!$price || $price == '₽ 0') {
            return null;
        }

        $str = explode(' ', $price);
        return str_replace(',', '', $str[1]);
    }
}


