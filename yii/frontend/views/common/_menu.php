<?php
use common\system\Helper;
use common\models\Categories;
use \yii\helpers\Url;

?>
<div class="navbar navbar-expand-md navbar-dark bg-dark box-shadow">
    <div class="container d-flex justify-content-between">
        <div class="navbar-brand d-flex align-items-center">
            <strong>Каталог товаров</strong>
        </div>
        <a href="#left-aside" class="onoffcanvas-toggler is-animated" data-toggle=onoffcanvas style="width:24px; height:24px;"></a>
    </div>
</div>

<div class="onoffcanvas is-fixed is-left bg-dark text-white left-aside p-1" id="left-aside">

    <div class="d-flex p-3 justify-content-end">
        <a href="#left-aside" class="onoffcanvas-toggler is-animated" data-toggle=onoffcanvas style="width:24px; height:24px;"></a>
    </div>
    <ul class="metismenu left-menu" id="menu1">
        <?php foreach ((new Categories())->getTree() as $item) : ?>
            <li class="">
                <a href="<?= Url::to(['/site/search/' . $item['category_id']]); ?>" class="has-arrow <?= Helper::getFromMap($item, 'children') ? 'drop' : '' ?>">
                    <span class="<?= $item['img'] ?> icons-category"></span><?= $item['name'] ?>
                </a>
                <?php if (Helper::getFromMap($item, 'children')) : ?>
                    <ul class="">
                        <?php foreach ($item['children'] as $i) : ?>
                            <li><a href="<?= Url::to(['/site/search/' . $i['category_id']]); ?>"><?= $i['name'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<script>
if (location.hostname === "onokumus.com") {
    (adsbygoogle = window.adsbygoogle || []).push({});
}

document.addEventListener("DOMContentLoaded", function (event) {
    const leftAside = document.getElementById("left-aside");
    const leftAsideCanvas = new OnoffCanvas(leftAside);

    const leftMenu = document.getElementById("menu1");
    const leftMetisMenu = new MetisMenu(leftMenu);
});
</script>
