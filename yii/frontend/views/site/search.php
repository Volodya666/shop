<?php
use kartik\select2\Select2;
use common\models\Offers;
use kartik\checkbox\CheckboxX;
use kartik\money\MaskMoney;
?>
<div class="panel panel-default padding-20 offer-wrap">
    <div class="panel-heading ">
        <div class="row">
            <div class="col-md-3">
                <h1 class="muted"><span class="fa fa-filter"></span> Филтер</h1>
            </div>
            <div class="col-md-9 filter-offer">
                <div class="col-md-3">
                    <?= Select2::widget([
                        'name' => 'kv_theme_select2',
                        'data' => (new Offers())->getTypeList(),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Тип товара', 'class' => 'param-type-offer'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <?= MaskMoney::widget([
                            'name' => 'amount_ph_1',
                            'options' => [
                                'placeholder' => 'Минимальная цена',
                                'class' => 'param-price-min'
                            ],
                            'pluginOptions' => [
                                'precision' => 0,
                                'prefix' => '₽ '
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= MaskMoney::widget([
                            'name' => 'amount_ph_2',
                            'options' => [
                                'placeholder' => 'максимальная цена',
                                'class' => 'param-price-max'
                            ],
                            'pluginOptions' => [
                                'precision' => 0,
                                'prefix' => '₽ '
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <?= Select2::widget([
                        'name' => 'kv_theme_select2_2',
                        'data' => (new Offers())->getPriceType(),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['class' => 'param-type-price'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="offers-wrap padding-20">

        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <div class="btn btn-success add-offer" style="margin: 0 auto;display: none">Показать еще</div>
            </div>
        </div>
    </div>
</div>

<?= $this->render("//common/_template_card", ['templateId' => 'tpl-search']) ?>
<?php $this->registerJsFile('/js/search.js'); ?>

<script>
    setTimeout(function () {
        offer.initVariable({
            value: '<?= $r ?>',
            categoryId: <?= $categoryId ?>
        });
    }, 0);
</script>
