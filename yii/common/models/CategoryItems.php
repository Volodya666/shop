<?php

namespace common\models;

use common\system\PModel;
use Yii;

/**
 * This is the model class for table "category_items".
 *
 * @property int $category_id
 * @property int $shop_category_id
 *
 * @property Categories $category
 * @property ShopCategories $shopCategory
 */
class CategoryItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'shop_category_id'], 'required'],
            [['category_id', 'shop_category_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'category_id']],
            [['shop_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategories::className(), 'targetAttribute' => ['shop_category_id' => 'shop_category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'shop_category_id' => 'Shop Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategory()
    {
        return $this->hasOne(ShopCategories::className(), ['shop_category_id' => 'shop_category_id']);
    }
}
