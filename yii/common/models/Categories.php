<?php

namespace common\models;

use common\system\Helper;
use common\system\PModel;
use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $category_id
 * @property string $created_at
 * @property string $name
 * @property string $img
 * @property int $parent_id
 * @property boolean $popular
 *
 * @property CategoryItems[] $categoryItems
 */
class Categories extends \yii\db\ActiveRecord
{

    const POPULAR = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
//            [['name', 'img', 'parent_id'], 'required'],
            [['parent_id'], 'integer'],
            [['name', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'created_at' => 'Created At',
            'name' => 'Name',
            'img' => 'Img',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryItems()
    {
        return $this->hasMany(CategoryItems::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $model = self::find()->all();

        $result = [];
        /** @var Categories $item */
        foreach ($model as $item) {
            $result['list'][$item->category_id] = $item->name;

            if ($item->popular) {
                $result['popular'][$item->category_id] = $item->name;
            }
        }

        return $result;
    }

    /**
     * @return array|mixed
     */
    public function getTree()
    {
        $items = self::find()->asArray()->all();
        return $this->buildTree($items);
    }

    /**
     * @param $elements
     * @param int $parentId
     * @return array
     */
    public function buildTree(array $elements, $parentId = 0)
    {
        $branch = [];
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['category_id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    /**
     * @param array $elements
     */
    public function buildTreeHtml(array $elements)
    {

        echo '<ul>';
        foreach ($elements as $element) {

            $popular = (self::POPULAR == $element['popular']) ? 'glyphicon glyphicon-star' : '';

            echo '<li data-jstree=\'{"icon":"' . $popular . '"}\' data-id = ' . $element['category_id'] . '>' . $element['name'];
            if (isset($element['children'])) {
                $this->buildTreeHtml($element['children']);
            }
            echo '</li><i class="' . $popular . '"></i>';
        }
        echo '</ul>';
    }

    /**
     * @return bool
     */
    public function popular()
    {
        $this->popular = (self::POPULAR == $this->popular) ? 0 : self::POPULAR;
        return $this->save();
    }
}
