<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shop_categories".
 *
 * @property int $shop_category_id
 * @property int $id
 * @property string $title
 * @property int $shop_id
 *
 * @property CategoryItems[] $categoryItems
 * @property Offers[] $offers
 * @property Shops $shop
 */
class ShopCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['id', 'title', 'shop_id'], 'required'],
//            [['id', 'shop_id'], 'integer'],
//            [['title'], 'string', 'max' => 255],
//            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shops::className(), 'targetAttribute' => ['shop_id' => 'shop_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shop_category_id' => 'Shop Category ID',
            'id' => 'ID',
            'title' => 'Title',
            'shop_id' => 'Shop ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryItems()
    {
        return $this->hasMany(CategoryItems::className(), ['shop_category_id' => 'shop_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offers::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shops::className(), ['shop_id' => 'shop_id']);
    }
}
