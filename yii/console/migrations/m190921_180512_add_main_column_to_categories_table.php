<?php

use yii\db\Migration;

/**
 * Handles adding main to table `{{%categories}}`.
 */
class m190921_180512_add_main_column_to_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'main', $this->boolean()->after('parent_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('categories', 'main');
    }
}
