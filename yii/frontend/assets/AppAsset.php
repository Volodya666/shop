<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/nprogress.css',
        'css/menu.css',
        'css/metismenujs.css',
        'css/onoffcanvas.min.css',
        'public/css/style.css',
        'public/css/colors/dark-green.css',
    ];

    public $js = [
        'js/underscore.js',
        'js/form.js',
        'js/nprogress.js',
        'js/head.js',
        'js/metismenujs.js',
        'js/onoffcanvas.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
