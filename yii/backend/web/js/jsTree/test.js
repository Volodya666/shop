$.jstree.defaults.core = {};

$('#tree1')
    .on('changed.jstree', function (event, data) {
        if (data.action == 'select_node') {
            $('#tree1').find('li').removeClass('disabled_node');
            currentlevel = parseInt($('#' + data.node.id).attr('aria-level'));
            $('#tree1').find('li').each(function () {
                if ($(this).attr('aria-level') > currentlevel) {
                    $(this).addClass('disabled_node');
                }
            });

        }
        if (data.action == 'deselect_node') {
            $('#tree1').find('li').each(function () {
                if ($(this).attr('aria-level') > currentlevel) {
                    $(this).removeClass('disabled_node');
                }
            });
        }
    })
    .jstree({
        "core": {
            "themes": {
                "url": true,
                "icons": true,
                "dots": true
            }
        },
        "plugins": ["dnd"]
    });


$('#tree2')
    .on('changed.jstree', function (event, data) {
        if (data.action == 'select_node') {
            $('#tree2').find('li').removeClass('disabled_node');
            currentlevel = parseInt($('#' + data.node.id).attr('aria-level'));
            $('#tree2').find('li').each(function () {
                if ($(this).attr('aria-level') > currentlevel) {
                    $(this).addClass('disabled_node');
                }
            });

        }
        if (data.action == 'deselect_node') {
            $('#tree2').find('li').each(function () {
                if ($(this).attr('aria-level') > currentlevel) {
                    $(this).removeClass('disabled_node');
                }
            });
        }
    })
    .jstree({
        "core": {
            "themes": {
                "url": true,
                "icons": 'file'
            },

            // 'check_callback': function (operation, node, node_parent, node_position, more) {
            //     if (operation === "move_node") {
            //         return false;
            //     }
            // }
        },
        // "types": {
        //     "root": {
        //         "icon" : "glyphicon glyphicon-plus"
        //     },
        //     "child": {
        //         "icon" : "glyphicon glyphicon-leaf"
        //     },
        //     "default" : {
        //     }
        // },
        // "plugins": ["dnd", 'types']
    });

$(".shopCategory").draggable();