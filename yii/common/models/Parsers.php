<?php
namespace common\models;

use common\system\Helper;

class Parsers extends Shops
{
    private $apiKey = 'fd23f641f2c851863fda19d0f90bd334';

    /**
     *
     */
    public function getParser()
    {
        $links = $this->getDepLink();
        $day = date('d', time());

        foreach ($links as $id => $link) {
            $model = new  ApiEpn($this->apiKey, $link);
            $model->AddRequestCountForSearch('category_count');
            $model->RunRequests();

            $p = $model->GetRequestResult('category_count');
            foreach (Helper::getFromMap($p, 'count', []) as $k => $v) {
                $model->clearResult();
                $model->clearRequests();
                $model->AddRequestSearch('test', ['limit' => $v, 'category' => $k, 'currency' => 'RUR', 'lang' => 'ru']);
                $model->RunRequests();
                $offers = $model->GetRequestResult('test');

                foreach (Helper::getFromMap($offers, 'offers', []) as $item) {
                    $update = Offers::find()->where(['id' => $item['id'], 'shop_id' => $id])->one();
                    $modelOffer = $update ? $update : new Offers();
                    $modelOffer->getParams($item, $id);
                    $modelOffer->save();

                    //
                    $offerIds = OffersId::find()->where(['offer_id' => $item['id'], 'shop_id' => $id])->one();
                    $modelOffersId = $offerIds ? $offerIds : new OffersId();
                    $modelOffersId->day = $day;
                    $modelOffersId->shop_id = $id;
                    $modelOffersId->offer_id = $item['id'];
                    $modelOffersId->save();
                }
            }

            $model->clearResult();
            $model->clearRequests();
            $model->AddRequestCategoriesList('category');
            $model->RunRequests();
            $categories = $model->GetRequestResult('category');
            foreach (Helper::getFromMap($categories, 'categories', []) as $items) {
                $update = ShopCategories::find()->where(['id' => $items['id'], 'shop_id' => $id])->one();
                $modelCategories = $update ? $update : new ShopCategories();
                $modelCategories->id = $items['id'];
                $modelCategories->shop_id = $id;
                $modelCategories->title = $items['title'];
                $modelCategories->save();
            }

            $model->clearResult();
            $model->clearRequests();
            $model->AddRequestGetTopMonthly('top');
            $model->RunRequests();
            $topList = $model->GetRequestResult('top');

            foreach (Helper::getFromMap($topList, 'offers', []) as $item) {
                $update = Offers::find()->where(['id' => $item['id'], 'shop_id' => $id])->one();
                $modelOffer = $update ? $update : new Offers();
                $modelOffer->getParams($item, $id, true);
                $modelOffer->save();
            }
        }

        /*
         * delete old rows
         */
        \Yii::$app
            ->db
            ->createCommand("delete `offers`  from `offers` INNER JOIN offers_id on offers.`id`= offers_id.`offer_id` and offers.shop_id = offers_id.shop_id where offers_id.`day`<> '$day'")
            ->execute();

        \Yii::$app
            ->db
            ->createCommand("delete FROM `offers_id` where `day` <> '$day'")
            ->execute();
    }
}