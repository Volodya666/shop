var custom = (function () {

    var wrapTree,
        ajaxUrl = '/api/changes-js-tree',
        popularButton = $('.popularButton');

    /**
     *
     */
    function init() {
        wrapTree = $('#data');
        bind();
        jsTree();
    }

    /**
     *
     */
    function bind() {
        $(document).on('dnd_stop.vakata', function (e, data) {
            saveNode(data);
        });
        popularButton.on('click', function () {
            var ajaxUrl = 'api/popular',
                categoryId = $("#" + wrapTree.jstree("get_selected")).attr('data-id'),
                data = {
                    categoryId: categoryId
                };
            ajaxRequest(data, ajaxUrl)
        });

    }

    /**
     *
     * @param data
     */
    function saveNode(data) {
        var node = wrapTree.jstree(true).get_node(data.data.nodes),
            parentId = node.parent == '#' ? null : $('#' + node.parent).data('id'),
            nodeId = node.data.id;
        ajaxRequest({parentId: parentId, nodeId: nodeId});
    }

    /**
     *
     * @param data
     * @param _ajaxUrl
     */
    function ajaxRequest(data, _ajaxUrl) {
        $.ajax({
            url: _ajaxUrl ? _ajaxUrl : ajaxUrl,
            data: data,
            success: function (resp) {

            },
            error: function (error) {
                console.error('Ошибка: ', error);
            }
        });

    }

    /**
     *
     */
    function jsTree() {
        wrapTree.jstree({
            'core': {
                "check_callback": function () {
                    return true;
                }
            },
            "types": {
                "max_depth": 2,
                "hover_node": false,
                "select_node": function () {
                    return false;
                }
            },
            "plugins": [
                "dnd", "themes", "html_data", "ui", "crrm", "contextmenu", "sort", "types"
            ],
            'contextmenu': function () {
                return true;
            }
        });
    }

    //
    $(document).ready(function () {
        init();
    });
})();