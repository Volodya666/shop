<?php
namespace frontend\controllers;

use backend\models\Banners;
use common\models\ApiEpn;
use common\models\Offers;
use common\models\Parsers;
use common\models\Shops;
use common\system\Helper;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $banners = (new Banners())->getList();
        $logos = (new Shops())->getLogoList();

        return $this->render('index', [
            'banners' => $banners, 'logos' => $logos,
        ]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionSearch($id = null)
    {
        $r = null;
        $post = Yii::$app->request->post();
        if (Helper::getFromMap($post, 'Offers') && Helper::getFromMap($post['Offers'], '_search')) {
            $r = $post['Offers']['_search'];
        }

        return $this->render('search', ['r' => $r, 'categoryId' => $id]);
    }
}
