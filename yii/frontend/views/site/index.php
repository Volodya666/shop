<?php
use kv4nt\owlcarousel\OwlCarouselWidget;
?>

<?php

OwlCarouselWidget::begin([
    'container' => 'div',
    'containerOptions' => [
        'id' => 'container-id',
        'class' => 'container-class'
    ],
    'pluginOptions' => [
        'autoplay' => true,
        'autoplayTimeout' => 3000,
        'items' => 4,
        'loop' => true,
        'itemsDesktop' => [1199, 3],
        'itemsDesktopSmall' => [979, 3]
    ]
]);

?>

<?php foreach ($banners as $banner) : ?>
    <div class="item-class"><?= $banner ?></div>
<?php endforeach; ?>

<?php OwlCarouselWidget::end(); ?>

<?= \ibrarturi\scrollup\ScrollUp::widget([
    'theme' => '',
    'options' => [
        'scrollName' => 'scrollUp',
        'scrollDistance' => 300,
        'scrollFrom' => 'top',
        'scrollSpeed' => 300,
        'easingType' => 'linear',
        'animation' => 'fade',
        'animationSpeed' => 200,
        'scrollTrigger' => false,
        'scrollTarget' => false,
        'scrollText' => '<span class="fa fa-space-shuttle fa-rotate-270 fa-4x"></span>',
        'scrollTitle' => false,
        'scrollImg' => false,
        'activeOverlay' => false,
        'zIndex' => 2147483647,
    ]
]); ?>

<?= $this->render('_new', ['modelsTop' => $modelsTop, 'pagesTop' => $pagesTop, 'logos' => $logos]); ?>
<?= $this->render('_sales', ['modelsSale' => $modelsSale, 'pagesSale' => $pagesSale, 'logos' => $logos]); ?>

    <div class="s-width">
        <div class="s-about">
            <div class="s-about-title">Как пользоваться сайтом?</div>
            <div style="" class="s-about-div">
                <div style="font-size: 17px; padding: 0 0 15px;">E-Katalog — многофункциональный сервис поиска товаров в
                    интернет-магазинах и сравнения цен. Он охватывает самые разнообразные категории товаров:
                    электроника, компьютеры, бытовая техника, автотовары, оборудование для ремонта и строительства,
                    туристическое снаряжение, детские товары и многое другое.
                </div>

                Наша задача - помочь покупателю быстро и удобно найти самое выгодное предложение. Для тех, кто
                определяется с выбором, в каждом разделе есть подбор по параметрам и возможность сравнить товары между
                собой. Доступен и удобный текстовый поиск, позволяющий искать как нужные разделы, так и конкретные
                товары по названию. А на странице каждой модели есть подробная информация, которая поможет принять
                решение: описание, технические характеристики, фото и видео, полезные ссылки и отзывы. Там же находится
                блок «Где купить?» со списком интернет-магазинов, ценами и прямыми ссылками на страницу покупки.<br><br>

                К системе E-Katalog подключено более 1000 магазинов, данные по которым постоянно обновляются. Благодаря
                этому вы сможете не только выбрать подходящий товар, но и купить его по самым выгодным условиям!
            </div>
        </div>
    </div>

    <div class="panel panel-default padding-20">
        <div class="panel-heading">
            <h1 class="muted"><span class="el el-friendfeed"></span> Наши партнеры</h1>
        </div>
        <div class="panel-body">
            <div class="row">
                <?php foreach ($logos as $logo) : ?>
                    <div class="col-md-3 partner-logo"><img class="partner-logo-img" src="<?= $logo ?>"></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

<?php $this->registerJsFile('/js/main.js'); ?>