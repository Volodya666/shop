var form = {

    /**
     * @param n
     * @param c
     * @param d
     * @param t
     * @param rubs
     * @param currency
     * @returns {string}
     */
    formatPrice: function (n, c, d, t, rubs, currency) {
        var c = c || 2, d = d || ',', t = t || ' ';
        c = isNaN(c = Math.abs(c)) ? 2 : c;
        d = d == undefined ? "." : d;
        t = t == undefined ? "," : t;
        var s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        var cur = 'руб.';
        if (currency) {
            cur = currency;
        }
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
            + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "")
            + ' ' + cur;
    },

    /**
     * @param n
     * @param cur
     * @returns {*|string}
     */
    formatPriceCurrency: function (n, cur) {
        var list = {RUR: 'руб.', USD: 'USD', EUR: 'EUR'},
            currency = list[cur];
        return this.formatPrice(n, false, false, false, false, currency);
    },

    /**
     * @param n
     * @returns {string}
     */
    formatPriceAlt: function (n) {
        var value = this.formatPrice(n, 2, ',', '').replace(this.currencyShort, '').replace(' ', '').trim();
        return (n) ? value : '';
    },

    /**
     * @param num
     * @returns {*}
     */
    roundDec: function (num) {
        if (!num) {
            return '';
        }
        return Math.round(num / 10) * 10;
    },

    /**
     * @param price
     * @returns {number}|{string}
     */
    clearPrice: function (price) {
        if (!price) {
            return '';
        }
        return price.split(' ').join('') * 1;
    },

    /**
     *
     * @param url
     * @param data
     * @param success
     * @param method
     */
    request: function (url, data, success, method) {
        success = success || {};

        var defaults = {
            method: method ? 'get' : 'post',
            url: url,
            data: data
        };
        var params = $.extend(defaults, success);
        $.ajax(params);
    },

    /**
     *
     * @param url
     * @param data
     * @param success
     */
    get: function (url, data, success) {
        form.request(url, data, success, true);
    },

    /**
     *
     * @param url
     * @param data
     * @param success
     */
    post: function (url, data, success) {
        form.request(url, data, success);
    },

    /**
     * @param position
     */
    tooltips: function(position) {
        position = position || 'top';
        $('.has-tooltip').tooltip({placement: position, container: "body"});
    }
};