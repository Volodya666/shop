<?php
/* @var $this yii\web\View */
use common\models\Categories;
?>
<? $this->registerCssFile(Yii::$app->request->BaseUrl . '/css/jsTree/style.min.css'); ?>
<div class="container">
    <div class="alert alert-danger" role="alert">
        <b>Информация!</b> меню должен быть двухуровневый
    </div>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-xs-4">
            <button class="btn btn-warning popularButton"><i class="glyphicon glyphicon-star"></i>Popular</button>
            <button class="btn btn-primary" id="showShopCategoryItems">Show Shop Category Items</button>
        </div>
        <div class="col-xs-2">
            <input class="form-control" id="categoryImg" type="text">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 wrap-tree">
            <div id="data" class="demo">
                <? (new Categories)->buildTreeHtml($tree); ?>
            </div>
        </div>

        <div class="col-sm-6 layer">
            <?php foreach ($shopCategories as $category): ?>
                <div class="shopCategory" id="<?=$category->shop_category_id?>"><?= $category->title; ?></div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<? $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jsTree/jstree.js'); ?>
<? $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jsTree/customJs.js'); ?>
<? $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/main.js'); ?>