/*!
 * onoffcanvas - v2.2.3
 * An offcanvas plugin
 * https://github.com/onokumus/onoffcanvas
 *
 * Made by onokumus <onokumus@gmail.com> (https://github.com/onokumus)
 * Under MIT License
 */
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):t.OnoffCanvas=e()}(this,function(){"use strict";
    /*! *****************************************************************************
     Copyright (c) Microsoft Corporation. All rights reserved.
     Licensed under the Apache License, Version 2.0 (the "License"); you may not use
     this file except in compliance with the License. You may obtain a copy of the
     License at http://www.apache.org/licenses/LICENSE-2.0

     THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
     WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
     MERCHANTABLITY OR NON-INFRINGEMENT.

     See the Apache Version 2.0 License for specific language governing permissions
     and limitations under the License.
     ***************************************************************************** */var s=function(){return(s=Object.assign||function(t){for(var e,n=1,i=arguments.length;n<i;n++)for(var r in e=arguments[n])Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t}).apply(this,arguments)},t=".onoffcanvas",n={HIDE:"hide"+t,SHOW:"show"+t},i="is-open",c='[data-toggle="onoffcanvas"]',d={createDrawer:!0,hideByEsc:!0};function h(t){var e=t.getAttribute("data-target");e&&"#"!==e||(e=t.getAttribute("href")||"");try{return 0<document.querySelectorAll(e).length?e:null}catch(t){throw new Error("Target Not Found!")}}var l=function(){function t(t,e){var n=this;this.element="string"==typeof t?document.querySelector(t):t,this.config=s({},d,e),this.triggerElements=document.querySelectorAll(c+'[href="#'+this.element.id+'"],\n      '+c+'[data-target="#'+this.element.id+'"]'),this.addAriaExpanded(this.triggerElements);for(var i=0,r=[].slice.call(this.triggerElements);i<r.length;i++){r[i].addEventListener("click",function(t){"A"===t.currentTarget.tagName&&t.preventDefault(),n.toggle()})}this.drawer=document.createElement("div"),this.drawer.classList.add("onoffcanvas-drawer"),document.documentElement.appendChild(this.drawer)}return t.autoinit=function(t){void 0===t&&(t=d);for(var e,n,i=document.querySelectorAll(""+c),r=function(t){for(var e=[],n=0,i=t;n<i.length;n++){var r=h(i[n]);e.push(r)}return e}([].slice.call(i)),s=0,o=r.filter(function(t,e,n){return e===n.indexOf(t)});s<o.length;s++){var a=o[s];e=a,n=t,void 0,new l(document.querySelector(e),n)}},t.prototype.on=function(t,e){return this.listen(t,e),this},t.prototype.toggle=function(){this.element.classList.contains(i)?this.hide():this.show()},t.prototype.show=function(){var e=this;this.element.classList.contains(i)||(this.element.classList.add(i),this.addAriaExpanded(this.triggerElements),this.emit(n.SHOW,this.element),this.config.createDrawer&&(this.drawer.classList.add("is-open"),this.drawer.addEventListener("click",this.hide.bind(this))),this.config.hideByEsc&&window.addEventListener("keydown",function(t){27===t.keyCode&&e.hide()}))},t.prototype.hide=function(){this.element.classList.contains(i)&&(this.config.createDrawer&&(this.drawer.classList.remove("is-open"),this.drawer.removeEventListener("click",this.hide.bind(this))),this.element.classList.remove(i),this.addAriaExpanded(this.triggerElements),this.emit(n.HIDE,this.element))},t.prototype.listen=function(t,e){return this.element.addEventListener(t,e,!1),this},t.prototype.emit=function(t,e,n){var i;return void 0===n&&(n=!1),"function"==typeof CustomEvent?i=new CustomEvent(t,{bubbles:n}):(i=document.createEvent("CustomEvent")).initCustomEvent(t,n,!1),this.element.dispatchEvent(i),this},t.prototype.addAriaExpanded=function(t){var n=this.element.classList.contains(i);Array.prototype.forEach.call(t,function(t,e){t.setAttribute("aria-expanded",n?"true":"false")})},t}();return l});
//# sourceMappingURL=onoffcanvas.min.js.map