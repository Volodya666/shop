<?php
use \yii\helpers\Html;

$id = isset($templateId) ? $templateId : '';
?>

<script type="text/template" id="<?= $id ?>">
    <div class="row search-row-wrap">
        <% for(var k in data) { %>
        <div class="col-md-2" >
            <div class="panel panel-default panel-custom">
                <div class="panel-body">
                    <div class="card">
                        <% if (data[k]['sales']) { %>
                        <div class="ribbons red"><span>sale</span></div>
                        <% } %>
                        <% if (!data[k]['sales'] && data[k]['new']) { %>
                        <div class="ribbons blues"><span>top</span></div>
                        <% } %>
                        <figure class="has-tooltip" title="<%= data[k]['name'] %>">
                            <img class="card-img-top" src="<%= data[k]['picture'] %> " width="100%">
                        </figure>

                        <div class="card-body card-body-custom">
                            <div class=" margin-0 class-display-block" style="text-align: center;">
                                <a href="<%= data[k]['url'] %>" target="_blank"
                                   class="has-tooltip link-shop"
                                   title="Перейти в магазин">
                                    <img class="card-brand-img" src="<%= data[k]['logos'] %>">
                                    <span class="glyphicon glyphicon-share-alt icons-link" aria-hidden="true"></span>
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class=" card-footer">
                                        <label style="display: block;">
                                            <span class="card-footer-2">
                                            <%= data[k]['sales'] ? form.formatPriceCurrency(data[k]['sale_price'], 'RUR') : form.formatPriceCurrency(data[k]['price'], 'RUR')  %>
                                            </span>
                                        </label>
                                        <label>
                                            <span class="card-footer-1">
                                            <%= form.formatPriceCurrency(data[k]['price'], 'RUR') %>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <% } %>
    </div>
</script>