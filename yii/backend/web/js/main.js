/**
 * Created by key on 26.10.2019.
 */
$(function () {
    var body = $('body'), idNode, layer = $(".layer"), categoryImg = $("#categoryImg");

    $(document).on('click', '.jstree-node', function () {
        idNode = $(this).data('id');
        $.ajax({
            url: '/api/get-category-img',
            data: {categoryId: idNode},
            success: function (resp) {
                categoryImg.val(resp);
            }
        });
    });

    categoryImg.blur(function () {
        var img = $(this).val();
        $.ajax({
            url: '/api/set-category-img',
            data: {categoryId: idNode, img: img},
            success: function (resp) {
            }
        });
    });

    $(".jstree-node").droppable({
        drop: function (event, ui) {
            var categoryId = $(event.target).attr('data-id'),
                shopCategoryId = ui.draggable.attr('id');

            $.ajax({
                url: '/api/set-category-items',
                data: {categoryId: categoryId, shopCategoryId: shopCategoryId},
                success: function (resp) {
                    if (resp) {
                        $('#' + shopCategoryId).hide();
                    }
                }
            });
        }
    });
    $(".shopCategory").draggable();

    $("#showShopCategoryItems").on('click', function () {

        $.ajax({
            url: '/api/get-category-items',
            data: {shopCategoryId: idNode},
            success: function (resp) {
                var html = showShopCategoryItems(resp);
                layer.html(html);
            }
        });

    });


    function showShopCategoryItems(_object) {
        var html = '';
        $.each(_object, function (key, value) {
            html += '<div class="shopCategory" id="' + value.shop_category_id + '">' + value.title + '</div>'
        });
        return html;
    }
});