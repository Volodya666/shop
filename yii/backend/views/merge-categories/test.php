<?php use common\models\Categories; ?>
<? $this->registerCssFile(Yii::$app->request->BaseUrl . '/css/jsTree/style.css'); ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div id="tree1" class="demo">
                    <? (new Categories)->buildTreeHtml($tree); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="tree" class="demo">
<!--                    <ul>-->
                        <?php foreach ($shopCategories as $spc): ?>
                            <div class="shopCategory"><?=$spc->title?></div>
                        <?php endforeach; ?>
<!--                    </ul>-->

                </div>
            </div>
        </div>
    </div>

<? $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jsTree/jstree.js'); ?>
<? $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jsTree/test.js'); ?>