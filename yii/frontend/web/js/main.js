var main = (function () {

    var wrapSales, wrapNew,
        salesNext, salesPrev, toggleNext, togglePrev,
        newNext, newPrev, toggleNextNew, togglePrevNew, search, wrapInput;

    /**
     *
     */
    function init() {
        initParams();
        bind();
        getNew();
        getSales();
    }

    /**
     *
     */
    function bind() {
        $(document).on('click', '.name-sales', function () {
            getSales();
        }).on('click', '.name-new', function () {
            getNew();
        }).on('click', '.sales-link-prev', function () {
            if ($(this).hasClass('color-hide')) {
                return;
            }
            prevSales();
        }).on('click', '.sales-link-next', function () {
            nextSales();
        }).on('click', '.new-link-prev', function () {
            if ($(this).hasClass('color-hide')) {
                return;
            }
            prevNew();
        }).on('click', '.new-link-next', function () {
            nextNew();
        }).on('keypress', '.select2-search__field', function () {
            var elem = $(this);
            setTimeout(function () {
                search = elem.val();
            }, 0)
        }).on('blur', '.select2-search__field', function () {
            wrapInput.append('<option value="' + search + '">' + search + '</option>');
            wrapInput.val(search);
        }).on('click', '.search-offers', function () {
            if (!wrapInput.val()) {
                return;
            }
            $('.input-search').val(wrapInput.val());
            $('#form-search').submit();
        });
    }

    /**
     *
     */
    function initParams() {
        salesNext = 1;
        salesPrev = 1;
        newNext = 1;
        newPrev = 1;
        wrapSales = $('.sales-wrap');
        wrapNew = $('.new-wrap');
        wrapInput = $('#w1');
    }

    /**
     *
     */
    function nextSales() {
        togglePrev = false;
        if (toggleNext) {
            salesNext = salesNext + 1;
        } else {
            salesNext = salesPrev + 1;
        }

        getSales(salesNext);
        toggleNext = true;
    }

    /**
     *
     */
    function prevSales() {
        toggleNext = false;
        if (togglePrev) {
            salesPrev = salesPrev - 1;
        } else {
            salesPrev = salesNext - 1;
        }

        if (salesPrev < 1) {
            salesPrev = 1;
        }

        getSales(salesPrev);
        togglePrev = true;
    }

    /**
     *
     */
    function prevNew() {
        toggleNextNew = false;
        if (togglePrevNew) {
            newPrev = newPrev - 1;
        } else {
            newPrev = newNext - 1;
        }

        if (newPrev < 1) {
            newPrev = 1;
        }

        getNew(newPrev);
        togglePrevNew = true;
    }

    /**
     *
     */
    function nextNew() {
        togglePrevNew = false;
        if (toggleNextNew) {
            newNext = newNext + 1;
        } else {
            newNext = newPrev + 1;
        }

        getNew(newNext);
        toggleNextNew = true;
    }

    /**
     *
     * @param params
     * @param page
     */
    function showSales(params, page) {
        var data = {data: params},
            html = _.template($('#tpl-sales').html(), data)(data);
        wrapSales.html(html);

        if (page == 1 || !page) {
            $('.sales-link-prev').prop("disabled", true).addClass('color-hide');
        } else {
            $('.sales-link-prev').prop("disabled", false).removeClass('color-hide');
        }

        form.tooltips();
    }

    /**
     *
     * @param page
     */
    function getSales(page) {
        var p = page ? {page: page} : {};
        form.get('/api/sales', p, {
            beforeSend: function () {
                $('.load-ajax-sales').addClass('loading');
            },
            success: function (data) {
                showSales(data, page);
                $('.load-ajax-sales').removeClass('loading');
            }
        });
    }

    /**
     *
     * @param page
     */
    function getNew(page) {
        var p = page ? {page: page} : {};
        form.get('/api/new', p, {
            beforeSend: function () {
                $('.load-ajax-new').addClass('loading');
            },
            success: function (data) {
                showNew(data, page);
                $('.load-ajax-new').removeClass('loading');
            }
        });
    }

    /**
     *
     * @param params
     * @param page
     */
    function showNew(params, page) {
        var data = {data: params},
            html = _.template($('#tpl-new').html(), data)(data);
        wrapNew.html(html);

        if (page == 1 || !page) {
            $('.new-link-prev').prop("disabled", true).addClass('color-hide');
        } else {
            $('.new-link-prev').prop("disabled", false).removeClass('color-hide');
        }

        form.tooltips();
    }

    //
    $(document).ready(function () {
        init();
    });

})();