<?php
namespace console\controllers;

use common\models\Parsers;
use yii\console\Controller;

class ParserController extends Controller
{

    /**
     *
     */
    public function actionRun()
    {
        echo 'START ';
        $start = microtime(true);
        (new Parsers())->getParser();
        $t = round(microtime(true) - $start);
        $t = round($t / 60);
        echo 'Время выполнения скрипта: ' . $t . ' мин.';
        echo ' END';
    }
}