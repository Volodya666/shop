<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "offers_id".
 *
 * @property int $offer_id_id
 * @property int $offer_id
 * @property int $day
 */
class OffersId extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_id';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offer_id_id' => 'Offer Id ID',
            'offer_id' => 'Offer ID',
            'day' => 'Day',
        ];
    }
}
