<?php

use yii\db\Migration;

/**
 * Class m190927_113123_add_order_column_in_categories_table
 */
class m190927_113123_add_order_column_in_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'order', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('categories', 'order');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_113123_add_order_column_in_categories_table cannot be reverted.\n";

        return false;
    }
    */
}
