<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property int $banner_id
 * @property string $banner
 * @property string $date_created
 * @property int $is_show
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['is_show'], 'integer'],
            [['banner'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'banner_id' => 'Banner ID',
            'banner' => 'Banner',
            'date_created' => 'Date Created',
            'is_show' => 'Is Show',
        ];
    }

    /**
     * @return array
     */
    public function getList()
    {
        $models = Banners::find()->where('is_show is true')->limit(4)->all();
        $result = [];

        /** @var Banners $model */
        foreach ($models as $model) {
            $result[] = $model->banner;
        }

        return $result;
    }
}
