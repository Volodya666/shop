<?php
namespace common\system;
use Yii;

class PModel extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class'=>'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate'=>false,
                'createAttribute'=>'created_at',
                'updateAttribute'=>'updated_at',
            ],
        ];
    }

}